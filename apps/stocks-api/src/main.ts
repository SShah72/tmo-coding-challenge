/**
 * This is not a production server yet!
 * This is only a minimal backend to get started.
 **/
import { Server } from 'hapi';
import axios from 'axios';

export interface HttpRequest<T> {
  url: string; 
  method: 'GET';
  headers: { [headerName: string]: string}
  timeout: number;
  data?: T; 
  params?: {}; 
}

export class HttpServiceFactory {
  static create() {
    axios.defaults.timeout = 2000;
    return axios; 
  }
}

export class RequestPayload {
  static requestObject(
    symbol: string | string[], 
    period: string | string[], 
    token: string | string[]
  ) {
    const request: HttpRequest<string> = {
      url: `https://sandbox.iexapis.com/beta/stock/${symbol}/chart/${period}?token=${token}`,
      method: 'GET',
      timeout: 3000,
      headers: {
        'Content-Type': 'application/json',
        Accept: 'application/json'
      }
    }; 
    return request; 
  }
}

const init = async () => {
  const server = new Server({
    port: 3333,
    host: 'localhost'
  });

  server.route({
    method: 'GET',
    path: '/hapi/',
    handler: async function (request, h) {
      const httpService = HttpServiceFactory.create();
      const response = await httpService(RequestPayload.requestObject(request.query.symbol, request.query.period, request.query.token));
      const resp = h.response(response.data);
      resp.ttl(30000)
      return {
        resp
      };
      options: {
        cache: {
          expiresIn: 30000
        }
      }
    }
  });

  await server.start();
  console.log('Server running on %s', server.info.uri);
};

process.on('unhandledRejection', err => {
  console.log(err);
  process.exit(1);
});

init();
